#Tweets mining & save tweets into couchDB
#Date: 14/April/2017
#Author:ccc2017-team20, Ke Shi

import couchdb
import tweepy
import json
from tweepy import OAuthHandler
from tweepy import Stream
from tweepy.streaming import StreamListener

#TweetStore
#initializing the database, 
#writing records to the database and
#reading records from the database
#the class initializer will either create a new database or 
#open an existing database if there already is one with the given name.
class TweetStore(object):
    def __init__(self, dbname, url='http://admin:admin@localhost:8080/'):
        try:
            self.server = couchdb.Server(url=url)
            self.db = self.server.create(dbname)
            #self._create_views()
        except couchdb.http.PreconditionFailed:
            self.db = self.server[dbname]
                  
    def save_tweet(self, tw):
        tw['_id'] = tw['id_str']
        if tw['_id'] in self.db:
           pass
	else:
           self.db.save(tw)

#mining tweets  
#step1: Go to apps.twitter.com
#create your own Twitter application credentials      
consumer_key = '3f1YoTUjFaW7hC51rBsaBPYnz'
consumer_secret = 'hxJPs3WK80XbNw1L8J9kPwagDJ1MaN65OPnXll6cmNfzCYfm6b'
access_token = '723474276691533824-dEPMe49akx7i0nc7ybasEDHcOfaFgrE'
access_secret = 'VBeQsDLEvUledz82pNi7cK4DZpinvL7bVj1Q4WrwuR5VT'

 
#consumer_key = 'KFQav3ccc5Z02WuxatMD1lncc'
#consumer_secret = 'hs8eeWkk6lYnRKDrBezwWJf7wGvNxE8Dx6rPoF8rr0IRbbRdiw'
#access_token = '723474276691533824-QJT8hBCMFo2RCah9se24JrTsKb96Ucl'
#access_secret = '15xOjDSVAD7fMpSngN5aLyICo9U6eq3DlQywJU1Ji6qlr' 
   
     
auth = OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_secret)

storage = TweetStore('meta_tweets_db')
api = tweepy.API(auth)

#tweets mining
#Starting a Stream
#--NOTE--#
#The track parameter is an array of search terms to stream.
#Streams not terminate unless the connection is closed, blocking the thread. 
#Tweepy offers a convenient async parameter on filter so the stream will run on a new thread.
class MyListener(StreamListener):
    def on_data(self, data):
        try:
            #store in a .json file, named 'tweets.json', just as backup
            with open('meta_tweets0.json', 'a') as f:
                #f.write(data)
                #convert unicode to json format
                data = json.loads(data)
                screenName = data['user']['screen_name']
                print screenName
                try:
                    timelines = api.user_timeline(screen_name = screenName, count = 3000, include_rts = True)
                    for tweet in timelines:
                        #if tweet._json['coordinates'] is not None:
                        #store in couchdb
                            storage.save_tweet(tweet._json)
                except Exception, e:
                    pass

                #store in couchdb
                #storage.save_tweet(data)
                return True
        except BaseException as e:
            print 'Exception: ', str(e)
        return True
    def on_error(self, status):
        print(status)
        return True
 
twitter_stream = Stream(auth, MyListener())

#Given geo location for Victoria
#Collect tweets within Vic 
twitter_stream.filter(locations=[141,-39,147,-36], async=True)

#####--END--#####
